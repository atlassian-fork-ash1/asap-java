package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.JwtClaims;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import javax.json.JsonObject;
import javax.json.JsonValue;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Matcher for JWT fields.
 * <p>
 * Verifies as much or as little as it is told to.
 * </p>
 */
final class JwtMatcher extends TypeSafeMatcher<Jwt> {
    private Optional<String> issuer = Optional.empty();
    private Optional<String> keyId = Optional.empty();
    private Optional<Optional<String>> subject = Optional.empty();
    private Optional<Set<String>> audience = Optional.empty();
    private Map<String, JsonValue> customClaims = new HashMap<>();
    private Duration expiration = JwtBuilder.DEFAULT_LIFETIME;

    private JwtMatcher() {
    }

    static JwtMatcher jwtMatcher() {
        return new JwtMatcher();
    }

    JwtMatcher issuer(String issuer) {
        this.issuer = Optional.of(issuer);
        return this;
    }

    JwtMatcher keyId(String keyId) {
        this.keyId = Optional.of(keyId);
        return this;
    }

    JwtMatcher subject(Optional<String> subject) {
        this.subject = Optional.of(subject);
        return this;
    }

    JwtMatcher audience(String... audience) {
        this.audience = Optional.of(ImmutableSet.copyOf(audience));
        return this;
    }

    JwtMatcher expiration(Duration duration) {
        this.expiration = requireNonNull(duration);
        return this;
    }

    JwtMatcher customClaim(String key, JsonValue expectedValue) {
        requireNonNull(key, "key");
        requireNonNull(expectedValue, "expectedValue");
        this.customClaims.put(key, expectedValue);
        return this;
    }

    @Override
    @SuppressWarnings("checkstyle:BooleanExpressionComplexity")
    protected boolean matchesSafely(Jwt jwt) {
        final JwtClaims claims = jwt.getClaims();
        final Duration expiration = Duration.between(claims.getIssuedAt(), claims.getExpiry());
        return matches(keyId, jwt.getHeader().getKeyId())
                && matches(issuer, claims.getIssuer())
                && matches(subject, claims.getSubject())
                && matches(audience, claims.getAudience())
                && this.expiration.equals(expiration)
                && hasAllExpectedCustomClaims(jwt);
    }

    private boolean hasAllExpectedCustomClaims(Jwt jwt) {
        final JsonObject json = jwt.getClaims().getJson();
        return customClaims.entrySet().stream()
                .allMatch(entry -> entry.getValue().equals(json.get(entry.getKey())));
    }

    @Override
    public void describeTo(Description description) {
        MoreObjects.ToStringHelper toString = MoreObjects.toStringHelper("Jwt");
        keyId.ifPresent(kid -> toString.add("kid", kid));
        issuer.ifPresent(iss -> toString.add("iss", iss));
        subject.ifPresent(sub -> toString.add("sub", sub));
        audience.ifPresent(aud -> toString.add("aud", aud));
        toString.add("exp-iat", expiration);
        if (!customClaims.isEmpty()) {
            toString.add("custom", customClaims);
        }
        description.appendText("Jwt with constraints ").appendValue(toString.toString());
    }

    private static <T> boolean matches(Optional<T> optionalExpectedValue, T actualValue) {
        return optionalExpectedValue
                .map(expected -> expected.equals(actualValue))
                .orElse(true);
    }
}
