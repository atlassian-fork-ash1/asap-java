package com.atlassian.asap.service.core.service;

import com.atlassian.asap.service.core.spi.AsapConfiguration;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;

import static com.atlassian.asap.service.core.service.EnvVarAsapConfiguration.ASAP_AUDIENCE;
import static com.atlassian.asap.service.core.service.EnvVarAsapConfiguration.ASAP_ISSUER;
import static com.atlassian.asap.service.core.service.EnvVarAsapConfiguration.ASAP_KEY_ID;
import static com.atlassian.asap.service.core.service.EnvVarAsapConfiguration.ASAP_PRIVATE_KEY;
import static com.atlassian.asap.service.core.service.EnvVarAsapConfiguration.ASAP_PUBLIC_KEY_REPOSITORY_URL;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class EnvVarAsapConfigurationTest {
    private static final String ISSUER = "micros/refapp";
    private static final String KEY_ID = "micros/refapp/key";
    private static final String AUDIENCE = "refapp";

    @Test
    public void constructWithEmptyEnv() {
        AsapConfiguration config = newConfig(ImmutableMap.of());
        assertThat(config.issuer(), is(ISSUER));
        assertThat(config.keyId(), is(KEY_ID));
        assertThat(config.audience(), is(AUDIENCE));
        assertThat(config.publicKeyRepositoryUrl(), is(""));
        assertThat(config.privateKeyUrl(), is(""));
    }

    @Test
    public void constructWithSettingsInEnv() {
        final String issuer = "fred";
        final String keyId = issuer + "/shhh";
        final String audience = "ginger";
        final String publicUrl = "http://public.example.com/";
        final String privateUrl = "http://private.example.com/";

        AsapConfiguration config = newConfig(ImmutableMap.of(
                ASAP_ISSUER, issuer,
                ASAP_KEY_ID, keyId,
                ASAP_AUDIENCE, audience,
                ASAP_PUBLIC_KEY_REPOSITORY_URL, publicUrl,
                ASAP_PRIVATE_KEY, privateUrl));

        assertThat(config.issuer(), is(issuer));
        assertThat(config.keyId(), is(keyId));
        assertThat(config.audience(), is(audience));
        assertThat(config.publicKeyRepositoryUrl(), is(publicUrl));
        assertThat(config.privateKeyUrl(), is(privateUrl));
    }

    @Test(expected = IllegalStateException.class)
    public void issuerMustMatchKeyIdWithEmptyEnv() {
        newConfig(ISSUER, "something/else", AUDIENCE);
    }

    @Test(expected = IllegalStateException.class)
    public void issuerMustMatchKeyIdWithOverrides() {
        newConfig(ISSUER, KEY_ID, AUDIENCE, ImmutableMap.of(ASAP_KEY_ID, "fred/shhh"));
    }


    private EnvVarAsapConfiguration newConfig(Map<String, String> env) {
        return newConfig(ISSUER, KEY_ID, AUDIENCE, env);
    }

    private EnvVarAsapConfiguration newConfig(String defaultIssuer, String defaultKeyId,
                                              String defaultAudience) {
        return newConfig(defaultIssuer, defaultKeyId, defaultAudience, ImmutableMap.of());
    }

    private EnvVarAsapConfiguration newConfig(String defaultIssuer, String defaultKeyId, String defaultAudience,
                                              Map<String, String> env) {
        return new EnvVarAsapConfiguration(defaultIssuer, defaultKeyId, defaultAudience,
                varName -> Optional.ofNullable(env.get(varName)));
    }
}
