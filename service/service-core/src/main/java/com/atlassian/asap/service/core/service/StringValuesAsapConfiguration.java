package com.atlassian.asap.service.core.service;

import com.atlassian.asap.service.core.spi.AsapConfiguration;
import com.google.common.base.MoreObjects;

import static java.util.Objects.requireNonNull;

public class StringValuesAsapConfiguration implements AsapConfiguration {
    private final String issuer;
    private final String keyId;
    private final String audience;
    private final String publicKeyRepositoryUrl;
    private final String privateKeyUri;

    public StringValuesAsapConfiguration(String issuer, String keyId, String audience,
                                         String publicKeyRepositoryUrl, String privateKeyUri) {
        this.issuer = requireNonNull(issuer, "issuer");
        this.keyId = requireNonNull(keyId, "keyId");
        this.audience = requireNonNull(audience, "audience");
        this.publicKeyRepositoryUrl = requireNonNull(publicKeyRepositoryUrl, "publicKeyRepositoryUrl");
        this.privateKeyUri = requireNonNull(privateKeyUri, "privateKeyUri");

        if (!keyId.startsWith(issuer)) {
            throw new IllegalStateException("The keyId '" + keyId + "' must include the issuer '" + issuer +
                    "' as a prefix");
        }
    }

    @Override
    public String issuer() {
        return issuer;
    }

    @Override
    public String keyId() {
        return keyId;
    }

    @Override
    public String audience() {
        return audience;
    }

    @Override
    public String publicKeyRepositoryUrl() {
        return publicKeyRepositoryUrl;
    }

    @Override
    public String privateKeyUrl() {
        return privateKeyUri;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("issuer", issuer)
                .add("keyId", keyId)
                .add("audience", audience)
                .add("publicKeyRepositoryUrl", publicKeyRepositoryUrl)
                .add("privateKeyUri", privateKeyUri.isEmpty() ? "blank" : "defined")
                .toString();
    }
}
