package com.atlassian.asap.service.core.spi;


/**
 * Configuration for the ASAP library, which provides the ability to create and verify authentication tokens
 * for service-to-service communication.
 *
 * @since 2.8
 */
public interface AsapConfiguration {
    /**
     * The issuer that is used when signing tokens.
     *
     * @return the default issuer
     */
    String issuer();

    /**
     * The key ID that is used when signing tokens.
     *
     * @return the default key ID
     */
    String keyId();

    /**
     * The default audience value that we expect to find when validating an authentication token that was sent to
     * by some other service.
     *
     * @return the default audience
     */
    String audience();

    /**
     * Where public keys can be found.
     *
     * @return the base URL of the public key repository
     */
    String publicKeyRepositoryUrl();

    /**
     * Where private keys can be found.
     *
     * @return the URL of our private key source
     */
    String privateKeyUrl();
}
