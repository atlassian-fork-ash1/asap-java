package com.atlassian.asap.service.api;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Point of contact of service-to-service authentication tools.
 *
 * @since 2.8
 */
public interface AsapService {
    /**
     * Creates a builder for generating an HTTP {@code Authorization} header value containing an ASAP token.
     *
     * @return a new authorization builder
     */
    AuthorizationBuilder authorizationBuilder();

    /**
     * Create a validator for verifying the contents of an HTTP {@code Authorization} header value.
     *
     * @return a new token validator
     */
    TokenValidator tokenValidator();

    /**
     * Creates a validator and populates it with the provided annotation's settings, then applies it
     * to the given {@code Authorization} header value.
     * <p>
     * This convenience method creates a {@link #tokenValidator() token validator}, applies all of
     * the settings from the annotation to it, then calls {@link TokenValidator#validate(Optional) validate}
     * using the supplied authorization header.
     * </p>
     *
     * @param annotation an {@code @AsapAuth} annotation with the settings to apply
     * @param authHeader the {@code Authorization} header that was provided for the request
     * @return the result of validating {@code authHeader} against the settings specified by {@code annotation}
     */
    default ValidationResult validate(AsapAuth annotation, Optional<String> authHeader) {
        requireNonNull(annotation, "annotation");
        requireNonNull(authHeader, "authHeader");
        return tokenValidator()
                .issuer(annotation.issuer())
                .subject(annotation.subject())
                .subjectImpersonation(annotation.subjectImpersonation())
                .impersonationIssuer(annotation.impersonationIssuer())
                .audience(annotation.audience())
                .policy(annotation.policy())
                .validate(authHeader);
    }
}
