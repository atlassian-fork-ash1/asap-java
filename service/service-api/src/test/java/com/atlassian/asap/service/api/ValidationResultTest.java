package com.atlassian.asap.service.api;

import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.Set;

import static com.atlassian.asap.service.api.ValidationResult.Decision.ABSTAIN;
import static com.atlassian.asap.service.api.ValidationResult.Decision.AUTHORIZED;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ValidationResultTest {
    @Test
    public void decisionIsOkReturnsExpectedResults() {
        final Set<ValidationResult.Decision> okDecisions = ImmutableSet.of(AUTHORIZED, ABSTAIN);
        for (ValidationResult.Decision decision : ValidationResult.Decision.values()) {
            assertThat(decision.name(), decision.isOk(), is(okDecisions.contains(decision)));
        }
    }
}
