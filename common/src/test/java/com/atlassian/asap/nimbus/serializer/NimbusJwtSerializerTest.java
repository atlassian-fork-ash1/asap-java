package com.atlassian.asap.nimbus.serializer;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.JwtClaims.RegisteredClaim;
import com.atlassian.asap.api.SigningAlgorithm;
import com.atlassian.asap.core.SecurityProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.util.Base64URL;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.json.Json;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.time.Instant;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NimbusJwtSerializerTest {
    private static final String KID = "my-key";
    private static final String ISSUER = "my-issuer";
    private static final String SUBJECT = "my-subject";
    private static final String AUDIENCE = "my-audience";
    private static final String TOKEN_ID = "my-id";
    private static final String SIGNATURE = "valid-signature";
    private static final long NOT_BEFORE_SECONDS = 1L;
    private static final long ISSUED_AT_SECONDS = 2L;
    private static final long EXPIRY_SECONDS = 3L;
    private static final Jwt BASE_JWT = JwtBuilder.newJwt()
            .algorithm(SigningAlgorithm.RS256)
            .keyId(KID)
            .issuer(ISSUER)
            .audience(AUDIENCE)
            .jwtId(TOKEN_ID)
            .issuedAt(Instant.ofEpochSecond(ISSUED_AT_SECONDS))
            .expirationTime(Instant.ofEpochSecond(EXPIRY_SECONDS))
            .notBefore(Optional.of(Instant.ofEpochSecond(NOT_BEFORE_SECONDS)))
            .build();

    @Mock
    private RSAPrivateKey rsaPrivateKey;
    @Mock
    private ECPrivateKey ecPrivateKey;
    @Mock
    private JWSSigner signer;

    @Test
    public void tokenShouldBeSignedWhenUsingRs256() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(SIGNATURE, signedJwsObject.getSignature().decodeToString());
        verify(signer).sign(any(JWSHeader.class), Matchers.<byte[]>any());
    }

    @Test
    public void tokenShouldBeSignedWhenUsingEs256() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(BASE_JWT).algorithm(SigningAlgorithm.ES256).build();
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(jwt, ecPrivateKey);

        assertEquals(SIGNATURE, signedJwsObject.getSignature().decodeToString());
        verify(signer).sign(any(JWSHeader.class), Matchers.<byte[]>any());
    }

    @Test
    public void tokenShouldIncludeKidInTheHeader() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(KID, signedJwsObject.getHeader().getKeyID());
    }

    @Test
    public void tokenShouldIncludeAlgorithmInTheHeader() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(JWSAlgorithm.RS256, signedJwsObject.getHeader().getAlgorithm());
    }

    @Test
    public void tokenShouldIncludeIssuerInThePayload() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(ISSUER, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.ISSUER.key()));
    }

    @Test
    public void tokenShouldIncludeSubjectInThePayloadIfSpecified() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(BASE_JWT).subject(Optional.of(SUBJECT)).build();
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(jwt, rsaPrivateKey);

        assertEquals(SUBJECT, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.SUBJECT.key()));
    }

    @Test
    public void tokenShouldNotIncludeSubjectInThePayloadIfNotSpecified() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertFalse(signedJwsObject.getPayload().toJSONObject().containsKey(RegisteredClaim.SUBJECT.key()));
    }

    @Test
    public void tokenShouldIncludeSingleAudienceInThePayload() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(AUDIENCE, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.AUDIENCE.key()));
    }

    @Test
    public void tokenShouldIncludeMultipleAudienceInThePayload() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(BASE_JWT).audience(AUDIENCE, "another-audience").build();
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(jwt, rsaPrivateKey);

        assertEquals(ImmutableList.of(AUDIENCE, "another-audience"),
                signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.AUDIENCE.key()));
    }

    @Test
    public void tokenShouldIncludeTokenIdInThePayload() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(TOKEN_ID, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.JWT_ID.key()));
    }

    @Test
    public void tokenShouldIncludeIssuedAtInThePayload() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(ISSUED_AT_SECONDS, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.ISSUED_AT.key()));
    }

    @Test
    public void tokenShouldIncludeExpiryInThePayload() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(EXPIRY_SECONDS, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.EXPIRY.key()));
    }

    @Test
    public void tokenShouldIncludeNotBeforeInThePayloadIfSpecified() throws Exception {
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(BASE_JWT, rsaPrivateKey);

        assertEquals(NOT_BEFORE_SECONDS, signedJwsObject.getPayload().toJSONObject().get(RegisteredClaim.NOT_BEFORE.key()));
    }

    @Test
    public void tokenShouldNotIncludeNotBeforeInThePayloadIfNotSpecified() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(BASE_JWT).notBefore(Optional.<Instant>empty()).build();
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(jwt, rsaPrivateKey);

        assertFalse(signedJwsObject.getPayload().toJSONObject().containsKey(RegisteredClaim.NOT_BEFORE.key()));
    }

    @Test
    public void tokenShouldIncludePrivateClaimInThePayloadIfSpecified() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(BASE_JWT)
                .customClaims(Json.createObjectBuilder().add("privateClaim", "privateClaimValue").build())
                .build();
        JWSObject signedJwsObject = createSerializer().getSignedJwsObject(jwt, rsaPrivateKey);

        assertEquals("privateClaimValue", signedJwsObject.getPayload().toJSONObject().get("privateClaim"));
    }

    private NimbusJwtSerializer createSerializer() throws Exception {
        when(signer.supportedJWSAlgorithms()).thenReturn(ImmutableSet.of(JWSAlgorithm.RS256, JWSAlgorithm.ES256));
        when(signer.sign(any(JWSHeader.class), Matchers.any())).thenReturn(Base64URL.encode(SIGNATURE));

        return new NimbusJwtSerializer(SecurityProvider.getProvider()) {
            @Override
            protected JWSSigner createRSASSASignerForKey(RSAPrivateKey privateKey) {
                // we extend this factory method to replace the collaborator with a mock
                return signer;
            }

            @Override
            protected JWSSigner createECDSASignerForKey(ECPrivateKey privateKey) {
                // we extend this factory method to replace the collaborator with a mock
                return signer;
            }
        };
    }
}
