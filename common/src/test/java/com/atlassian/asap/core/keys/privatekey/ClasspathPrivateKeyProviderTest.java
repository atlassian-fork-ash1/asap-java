package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;

import static com.atlassian.asap.core.keys.ClassPathUri.classPathUri;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClasspathPrivateKeyProviderTest {
    public static final String PRIVATE_KEY_BASE_PATH = "/privatekeys/";
    public static final String VALID_KID = "issuer1/rsa-key-for-tests";

    @Mock
    private PemReader pemReader;
    @Mock
    private RSAPrivateKey privateKey;

    @Test
    public void shouldBeAbleToReadKeyFromClasspathResource() throws Exception {
        KeyProvider<PrivateKey> keyRetriever = new ClasspathPrivateKeyProvider(PRIVATE_KEY_BASE_PATH, pemReader);
        when(pemReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);

        assertSame(privateKey, keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID)));
    }

    @Test
    public void shouldBeAbleToReadKeyFromSpecificClasspathResource() throws Exception {
        String customClasspathBase = "/custom/keylocation/";
        String customKeyId = "issuer3/rsa-key-for-tests";

        KeyProvider<PrivateKey> keyRetriever = new ClasspathPrivateKeyProvider(customClasspathBase, pemReader);
        when(pemReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);

        assertSame(privateKey, keyRetriever.getKey(ValidatedKeyId.validate(customKeyId)));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldGetErrorWhenKeyParsingFails() throws Exception {
        KeyProvider<PrivateKey> keyRetriever = new ClasspathPrivateKeyProvider(PRIVATE_KEY_BASE_PATH, pemReader);
        when(pemReader.readPrivateKey(any(InputStreamReader.class)))
                .thenThrow(new CannotRetrieveKeyException("Random error", classPathUri(PRIVATE_KEY_BASE_PATH + VALID_KID)));

        keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldGetErrorWhenClasspathResourceDoesNotExist() throws Exception {
        KeyProvider<PrivateKey> keyRetriever = new ClasspathPrivateKeyProvider(PRIVATE_KEY_BASE_PATH, pemReader);

        keyRetriever.getKey(ValidatedKeyId.validate("non-existent"));
    }
}
