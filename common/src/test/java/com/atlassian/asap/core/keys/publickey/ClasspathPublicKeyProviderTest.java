package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStreamReader;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClasspathPublicKeyProviderTest {
    public static final String PUBLIC_KEY_BASE_PATH = "/publickeyrepo/";
    public static final String VALID_KID = "issuer1/rsa-key-for-tests";

    @Mock
    private PemReader pemReader;
    @Mock
    private RSAPublicKey privateKey;

    @Test
    public void shouldBeAbleToReadKeyFromClasspathResource() throws Exception {
        KeyProvider<PublicKey> keyRetriever = new ClasspathPublicKeyProvider(PUBLIC_KEY_BASE_PATH, pemReader);
        when(pemReader.readPublicKey(any(InputStreamReader.class))).thenReturn(privateKey);

        assertSame(privateKey, keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID)));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldGetErrorWhenKeyParsingFails() throws Exception {
        KeyProvider<PublicKey> keyRetriever = new ClasspathPublicKeyProvider(PUBLIC_KEY_BASE_PATH, pemReader);
        when(pemReader.readPublicKey(any(InputStreamReader.class)))
                .thenThrow(new CannotRetrieveKeyException("Random error"));

        keyRetriever.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldGetErrorWhenClasspathResourceDoesNotExist() throws Exception {
        KeyProvider<PublicKey> keyRetriever = new ClasspathPublicKeyProvider(PUBLIC_KEY_BASE_PATH, pemReader);

        keyRetriever.getKey(ValidatedKeyId.validate("non-existent"));
    }

}
