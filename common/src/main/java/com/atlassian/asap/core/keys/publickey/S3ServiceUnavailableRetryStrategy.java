package com.atlassian.asap.core.keys.publickey;

import com.google.common.base.Preconditions;
import org.apache.http.HttpResponse;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.protocol.HttpContext;

/**
 * A strategy that follows the best practices defined by Amazon for S3 buckets.
 *
 * <p><a href="http://docs.aws.amazon.com/AmazonS3/latest/dev/ErrorBestPractices.html">Amazon S3 error handling best
 * practices</a> recommend to retry any 5xx error when talking to S3. The
 * {@link org.apache.http.impl.client.DefaultServiceUnavailableRetryStrategy} provided by the Apache HTTP client
 * library does not cover all 5xx errors, only 503.
 *
 * <p>This class does not currently implement exponential backoff or jitter.
 */
public class S3ServiceUnavailableRetryStrategy implements ServiceUnavailableRetryStrategy {
    private final int maxRetries;
    private final long retryInterval;

    /**
     * @param maxRetries    maximum number of attempts to make the request
     * @param retryInterval interval (in ms) to wait between retries
     */
    public S3ServiceUnavailableRetryStrategy(int maxRetries, long retryInterval) {
        Preconditions.checkArgument(maxRetries >= 0, "maxRetries must be non-negative");
        Preconditions.checkArgument(retryInterval >= 0, "retryInterval must be non-negative");

        this.maxRetries = maxRetries;
        this.retryInterval = retryInterval;
    }

    @Override
    public boolean retryRequest(HttpResponse response, int executionCount, HttpContext context) {
        return response.getStatusLine().getStatusCode() / 100 == 5 &&
                executionCount <= maxRetries;
    }

    @Override
    public long getRetryInterval() {
        return retryInterval;
    }
}
