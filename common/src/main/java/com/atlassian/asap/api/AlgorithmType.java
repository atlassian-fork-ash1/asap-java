package com.atlassian.asap.api;

public enum AlgorithmType {
    RSA("RSA"), // RSASSA-PKCS1-V1_5
    ECDSA("EC"), // Elliptic Curve Algorithm
    RSASSA_PSS("RSASSA-PSS");  // RSASSA-PSS PKCS# 1 v 2.1.

    /**
     * the standard name to use with the cryptographic provider.
     */
    private final String algorithmName;

    AlgorithmType(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public String algorithmName() {
        return algorithmName;
    }
}

