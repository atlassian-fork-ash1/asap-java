#!/bin/sh

# Add atlassian maven profiles, and maven servers, along with credentials for accessing them
# in the form of environment variables

sed --in-place='.bak' --file=- /usr/share/maven/conf/settings.xml<<END_SED
/<servers>/ r script/build/etc/servers.xml-fragment
/<profiles>/ r script/build/etc/profiles.xml-fragment
END_SED
