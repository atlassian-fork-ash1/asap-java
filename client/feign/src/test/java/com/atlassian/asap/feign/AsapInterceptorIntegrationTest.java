package com.atlassian.asap.feign;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import feign.Feign;
import feign.RequestLine;
import feign.gson.GsonDecoder;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import java.net.URI;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AsapInterceptorIntegrationTest {
    @Rule
    public MockServerRule mockServerRule = new MockServerRule(this);

    private MockServerClient mockServerClient;

    @Test
    public void shouldIncludeTokenInTheRequest() {
        // client
        AuthorizationHeaderGenerator authorizationHeaderGenerator =
                AuthorizationHeaderGeneratorImpl.createDefault(URI.create("classpath:/privatekeys/"));
        Jwt jwtPrototype = JwtBuilder.newJwt()
                .audience("audience").issuer("issuer1").keyId("issuer1/rsa-key-for-tests")
                .build();
        Client client = Feign.builder()
                .decoder(new GsonDecoder())
                .requestInterceptor(new AsapInterceptor(authorizationHeaderGenerator, jwtPrototype))
                .target(Client.class, "http://localhost:" + mockServerRule.getPort());

        // server
        HttpRequest expectedRequest = HttpRequest.request().withPath("/hello");
        mockServerClient
                .when(expectedRequest)
                .respond(HttpResponse.response().withStatusCode(200));

        // request
        client.hello();

        // assertions
        HttpRequest[] actualRequests = mockServerClient.retrieveRecordedRequests(expectedRequest);
        assertThat(actualRequests.length, is(1));
        assertThat(actualRequests[0].getFirstHeader("Authorization"), Matchers.startsWith("Bearer "));
    }

    interface Client {
        @RequestLine("GET /hello")
        String hello();
    }
}
