package com.atlassian.asap.core.client.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AsapAuthenticationFilterTest {
    private static final String AUDIENCE = "my-audience";
    private static final String ISSUER = "my-issuer";
    private static final String KEY_ID = "my-key-id";
    private static final String AUTHORIZATION_HEADER_VALUE = "some-value";

    @Rule
    public MockitoRule mockitoJunitRule = MockitoJUnit.rule();

    @Mock
    private AuthorizationHeaderGenerator authorizationHeaderGenerator;
    @Mock
    private ClientRequest clientRequest;
    @Mock
    private MultivaluedMap<String, Object> headers;

    private AsapAuthenticationFilter asapAuthenticationFilter;

    @Before
    public void setup() {
        when(clientRequest.getHeaders()).thenReturn(headers);
        asapAuthenticationFilter = new AsapAuthenticationFilter(
                JwtBuilder.newJwt()
                        .audience(AUDIENCE)
                        .issuer(ISSUER)
                        .keyId(KEY_ID)
                        .build(),
                authorizationHeaderGenerator);
    }

    @Test
    public void shouldAddHeaderIfDoesNottExist() throws Exception {
        when(headers.containsKey(HttpHeaders.AUTHORIZATION)).thenReturn(false);
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenReturn(AUTHORIZATION_HEADER_VALUE);

        asapAuthenticationFilter.handleInternal(clientRequest);

        verify(headers).add(eq(HttpHeaders.AUTHORIZATION), eq(AUTHORIZATION_HEADER_VALUE));
        verify(authorizationHeaderGenerator).generateAuthorizationHeader(any(Jwt.class));
    }

    @Test
    public void shouldNotAddHeaderIfItExists() throws Exception {
        when(headers.containsKey(HttpHeaders.AUTHORIZATION)).thenReturn(true);

        asapAuthenticationFilter.handleInternal(clientRequest);

        verify(headers, never()).add(anyString(), anyString());
        verify(authorizationHeaderGenerator, never()).generateAuthorizationHeader(any(Jwt.class));
    }

    @Test
    public void shouldThrowClientHandlderExceptionWhenTokenCanNotBeGenerated() throws Exception {
        when(headers.containsKey(HttpHeaders.AUTHORIZATION)).thenReturn(false);
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenThrow(new CannotRetrieveKeyException(""));

        try {
            asapAuthenticationFilter.handleInternal(clientRequest);
            fail("Should have thrown");
        } catch (ClientHandlerException e) {
            verify(headers, never()).add(anyString(), anyString());
            verify(authorizationHeaderGenerator).generateAuthorizationHeader(any(Jwt.class));
        }
    }
}
