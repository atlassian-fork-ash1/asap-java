package com.atlassian.asap.core.client.http;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.JwtConstants;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.privatekey.PrivateKeyProviderFactory;
import com.atlassian.asap.core.serializer.JwtSerializer;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.atlassian.asap.nimbus.serializer.NimbusJwtSerializer;

import java.net.URI;
import java.security.PrivateKey;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.asap.core.keys.ChainedKeyProvider.createChainedKeyProvider;
import static com.atlassian.asap.core.keys.privatekey.PrivateKeyProviderFactory.createPrivateKeyProvider;

public class AuthorizationHeaderGeneratorImpl implements AuthorizationHeaderGenerator {
    private final JwtSerializer jwtSerializer;
    private final KeyProvider<PrivateKey> privateKeyProvider;

    /**
     * Creates a new instance of {@link AuthorizationHeaderGeneratorImpl}.
     *
     * @param jwtSerializer      the serializer to use for signing and serializing a JWT object
     * @param privateKeyProvider the key provider to use for retrieving private keys used in signing
     */
    public AuthorizationHeaderGeneratorImpl(JwtSerializer jwtSerializer, KeyProvider<PrivateKey> privateKeyProvider) {
        this.jwtSerializer = Objects.requireNonNull(jwtSerializer);
        this.privateKeyProvider = Objects.requireNonNull(privateKeyProvider);
    }

    /**
     * Constructs a default instance for the given private key path.
     *
     * @param privateKeyPath location of the private keys
     * @return a new instance of the header generator
     */
    public static AuthorizationHeaderGenerator createDefault(URI privateKeyPath) {
        KeyProvider<PrivateKey> keyProvider = createPrivateKeyProvider(privateKeyPath);
        return new AuthorizationHeaderGeneratorImpl(new NimbusJwtSerializer(), keyProvider);
    }

    /**
     * Constructs a default instance for the given private key paths.
     *
     * @param privateKeyPaths location of the private keys
     * @return a new instance of the header generator
     */
    public static AuthorizationHeaderGenerator createDefault(URI... privateKeyPaths) {
        final List<KeyProvider<PrivateKey>> keyProviders = Stream.of(privateKeyPaths)
                .map(PrivateKeyProviderFactory::createPrivateKeyProvider)
                .collect(Collectors.toList());
        return new AuthorizationHeaderGeneratorImpl(new NimbusJwtSerializer(), createChainedKeyProvider(keyProviders));
    }


    @Override
    public String generateAuthorizationHeader(Jwt jwt) throws InvalidTokenException, CannotRetrieveKeyException {
        ValidatedKeyId validatedKeyId = ValidatedKeyId.validate(jwt.getHeader().getKeyId());

        PrivateKey privateKey = privateKeyProvider.getKey(validatedKeyId);

        return JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX + jwtSerializer.serialize(jwt, privateKey);
    }
}
