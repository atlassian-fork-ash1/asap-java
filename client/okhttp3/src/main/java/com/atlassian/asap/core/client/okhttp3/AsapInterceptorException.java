package com.atlassian.asap.core.client.okhttp3;

/**
 * Thrown when generating the ASAP "Authorization header fails in {@link AsapInterceptor}.
 *
 * @since 2.15
 */
public class AsapInterceptorException extends RuntimeException {

    public AsapInterceptorException(String message, Throwable cause) {
        super(message, cause);
    }
}
