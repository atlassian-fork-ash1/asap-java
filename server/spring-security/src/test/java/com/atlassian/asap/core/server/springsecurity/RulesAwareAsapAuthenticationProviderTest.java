package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.core.validator.JwtValidator;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.atlassian.asap.core.server.springsecurity.RulesAwareAsapAuthenticationProvider.grantIfSubjectMatches;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;

@RunWith(MockitoJUnitRunner.class)
public class RulesAwareAsapAuthenticationProviderTest {
    private static final String VALID_ISSUER = "valid-issuer";
    private static final Collection<GrantedAuthority> AUTHORITIES =
            Collections.singleton(new SimpleGrantedAuthority("auth"));
    private static final Jwt VALID_TOKEN = JwtBuilder.newJwt()
            .issuer(VALID_ISSUER).audience("aud").keyId("valid-issuer/key1")
            .build();

    @Mock
    private JwtValidator jwtValidator;

    @Test
    public void shouldGrantAuthoritiesForRecognisedJwt() {
        Map<String, Function<Jwt, Collection<GrantedAuthority>>> rules = ImmutableMap.of(
            VALID_ISSUER, (jwt) -> AUTHORITIES
        );

        RulesAwareAsapAuthenticationProvider rulesAuthProvider =
                new RulesAwareAsapAuthenticationProvider(jwtValidator, rules);

        assertThat(rulesAuthProvider.getGrantedAuthorities(VALID_TOKEN), is(AUTHORITIES));
    }

    @Test
    public void shouldNotGrantAuthoritiesForUnrecognisedJwt() {
        Map<String, Function<Jwt, Collection<GrantedAuthority>>> rules = ImmutableMap.of();

        RulesAwareAsapAuthenticationProvider rulesAuthProvider =
                new RulesAwareAsapAuthenticationProvider(jwtValidator, rules);

        assertThat(rulesAuthProvider.getGrantedAuthorities(VALID_TOKEN), empty());
    }

    @Test
    public void shouldGrantAuthoritiesForRecognisedJwtWithMatchingSubject() {
        Map<String, Function<Jwt, Collection<GrantedAuthority>>> rules = ImmutableMap.of(
                VALID_ISSUER, grantIfSubjectMatches(AUTHORITIES)
        );

        RulesAwareAsapAuthenticationProvider rulesAuthProvider =
                new RulesAwareAsapAuthenticationProvider(jwtValidator, rules);

        assertThat(rulesAuthProvider.getGrantedAuthorities(VALID_TOKEN), is(AUTHORITIES));
    }

    @Test
    public void shouldNotGrantAuthoritiesForRecognisedJwtWithNonMatchingSubject() {
        Jwt invalidToken = JwtBuilder.newJwt()
                .issuer(VALID_ISSUER).subject(Optional.of("invalid-subject")).audience("aud").keyId("valid-issuer/key1")
                .build();
        Map<String, Function<Jwt, Collection<GrantedAuthority>>> rules = ImmutableMap.of(
                VALID_ISSUER, grantIfSubjectMatches(AUTHORITIES)
        );

        RulesAwareAsapAuthenticationProvider rulesAuthProvider =
                new RulesAwareAsapAuthenticationProvider(jwtValidator, rules);

        assertThat(rulesAuthProvider.getGrantedAuthorities(invalidToken), empty());
    }
}
