package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.core.validator.JwtValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class IssuerAndSubjectWhitelistAsapAuthenticationProviderTest {
    private static final Collection<String> VALID_ISSUERS = Collections.singleton("valid-issuer");
    private static final Collection<String> VALID_SUBJECTS = Collections.singleton("valid-subject");
    private static final Collection<GrantedAuthority> AUTHORITIES =
            Collections.singleton(new SimpleGrantedAuthority("auth"));
    private static final Jwt VALID_TOKEN = JwtBuilder.newJwt()
            .issuer("valid-issuer").subject(Optional.of("valid-subject")).audience("aud").keyId("valid-issuer/key1")
            .build();

    @Mock
    private JwtValidator jwtValidator;

    private IssuerAndSubjectWhitelistAsapAuthenticationProvider sut;

    @Before
    public void createSut() {
        sut = new IssuerAndSubjectWhitelistAsapAuthenticationProvider(jwtValidator, VALID_ISSUERS, VALID_SUBJECTS, AUTHORITIES);
    }

    @Test
    public void shouldGrantAuthoritiesIfIssuerAndSubjectAreWhitelisted() {
        assertThat(sut.getGrantedAuthorities(VALID_TOKEN), equalTo(AUTHORITIES));
    }

    @Test
    public void shouldGrantNoAuthoritiesIfIssuerIsNotWhitelisted() {
        Jwt jwt = JwtBuilder.copyJwt(VALID_TOKEN).issuer("not-valid-issuer").build();
        assertThat(sut.getGrantedAuthorities(jwt), emptyCollectionOf(GrantedAuthority.class));
    }

    @Test
    public void shouldGrantNoAuthoritiesIfSubjectIsNotWhitelisted() {
        Jwt jwt = JwtBuilder.copyJwt(VALID_TOKEN).subject(Optional.of("not-valid-subject")).build();
        assertThat(sut.getGrantedAuthorities(jwt), emptyCollectionOf(GrantedAuthority.class));
    }
}
