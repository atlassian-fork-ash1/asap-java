package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.core.validator.JwtValidator;
import com.google.common.collect.ImmutableSet;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * ASAP authentication provider that grants static authorities based on whitelists of issuers and effective subjects.
 * Valid tokens that are not included in the whitelists will be authenticated, but are not granted any authority.
 *
 * <p>For a more flexible solution, please see {@link RulesAwareAsapAuthenticationProvider}.
 *
 * @see AsapAuthenticationProvider base class documentation for more
 */
public class IssuerAndSubjectWhitelistAsapAuthenticationProvider extends AsapAuthenticationProvider {
    private final Set<String> validIssuers;
    private final Set<String> validSubjects;
    private final Collection<GrantedAuthority> authorities;

    /**
     * @param jwtValidator  the validator of JWT tokens
     * @param validIssuers  a white list of valid issuers
     * @param validSubjects a white list of valid subjects
     * @param authorities   the authorities granted to tokens that are included in both whitelists
     */
    public IssuerAndSubjectWhitelistAsapAuthenticationProvider(JwtValidator jwtValidator,
                                                               Iterable<String> validIssuers,
                                                               Iterable<String> validSubjects,
                                                               Iterable<GrantedAuthority> authorities) {
        super(jwtValidator);
        this.validIssuers = ImmutableSet.copyOf(validIssuers);
        this.validSubjects = ImmutableSet.copyOf(validSubjects);
        this.authorities = ImmutableSet.copyOf(authorities);
    }

    @Override
    protected Collection<GrantedAuthority> getGrantedAuthorities(Jwt validJwt) {
        if (validIssuers.contains(validJwt.getClaims().getIssuer()) &&
                validSubjects.contains(effectiveSubject(validJwt))) {
            return authorities;
        } else {
            return Collections.emptyList();
        }
    }
}
