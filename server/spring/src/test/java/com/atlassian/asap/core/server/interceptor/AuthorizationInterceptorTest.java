package com.atlassian.asap.core.server.interceptor;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationInterceptorTest {

    @Mock
    private AsapValidator asapValidator;

    @InjectMocks
    private AuthorizationInterceptor authorizationInterceptor;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    private HandlerMethod securedMethod;
    private HandlerMethod unsecuredMethod;
    private HandlerMethod nonMandatoryAsap;

    @Before
    public void setUp() throws Exception {
        Controller controller = new Controller();
        unsecuredMethod = new HandlerMethod(controller, controller.getClass().getMethod("unsecuredMethod"));
        securedMethod = new HandlerMethod(controller, controller.getClass().getMethod("securedMethod"));
        nonMandatoryAsap = new HandlerMethod(controller, controller.getClass().getMethod("nonMandatoryAsap"));
    }

    @Test
    public void shouldAllowAccessToMethodWhenNotAnnotatedWithAsap() throws Exception {
        boolean preHandleResult = authorizationInterceptor.preHandle(request, response, unsecuredMethod);

        assertThat(preHandleResult, is(true));
    }

    @Test
    public void shouldAllowAccessToMethodWhenAnnotatedWithAsapThatIsNotMandatory() throws Exception {
        boolean preHandleResult = authorizationInterceptor.preHandle(request, response, nonMandatoryAsap);

        assertThat(preHandleResult, is(true));
    }

    @Test
    public void shouldNotAllowAccessToMethodAndReturn401WhenAnnotatedWithAsapAndNoHeaderInRequest() throws Exception {
        boolean preHandleResult = authorizationInterceptor.preHandle(request, response, securedMethod);

        assertThat(preHandleResult, is(false));
        verify(response).sendError(eq(401), anyString());
    }

    @Test
    public void shouldNotAllowAccessToMethodAndReturn403WhenAsapIssuerIsInvalid() throws Exception {
        Jwt jwt = mock(Jwt.class);
        JwtClaims claims = mock(JwtClaims.class);

        when(request.getAttribute(AUTHENTIC_JWT_REQUEST_ATTRIBUTE)).thenReturn(jwt);
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("token");

        when(jwt.getClaims()).thenReturn(claims);
        when(claims.getJwtId()).thenReturn("123");

        Mockito.doThrow(new AuthorizationFailedException("get out")).when(asapValidator).validate(anyObject(), anyObject());

        boolean preHandleResult = authorizationInterceptor.preHandle(request, response, securedMethod);

        assertThat(preHandleResult, is(false));
        verify(response).sendError(eq(403), anyString());
    }

    @Test
    public void shouldAuthenticateAndAuthorizeRequest() throws Exception {
        Jwt token = mock(Jwt.class);
        JwtClaims jwtClaims = mock(JwtClaims.class);

        when(request.getAttribute(AUTHENTIC_JWT_REQUEST_ATTRIBUTE)).thenReturn(token);
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("jwt header");
        when(token.getClaims()).thenReturn(jwtClaims);

        boolean preHandleResult = authorizationInterceptor.preHandle(request, response, securedMethod);
        assertThat(preHandleResult, is(true));

        verify(asapValidator).validate(securedMethod.getMethodAnnotation(Asap.class), token);
    }

    private static class Controller {
        public void unsecuredMethod() { }

        @Asap
        public void securedMethod() { }

        @Asap(mandatory = false)
        public void nonMandatoryAsap() { }
    }
}