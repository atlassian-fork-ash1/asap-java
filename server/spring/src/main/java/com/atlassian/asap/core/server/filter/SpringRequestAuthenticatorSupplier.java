package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.http.RequestAuthenticatorFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterConfig;
import java.util.Optional;
import java.util.function.Supplier;


class SpringRequestAuthenticatorSupplier implements Supplier<RequestAuthenticator> {
    public static final String SPRING_CONTEXT_NAME_INIT_PARAM_KEY = "springContextName";

    private final WebApplicationContext springContext;

    SpringRequestAuthenticatorSupplier(FilterConfig filterConfig) {
        springContext = WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext(), getAttributeName(filterConfig));
    }

    private String getAttributeName(FilterConfig filterConfig) {
        return StringUtils.defaultIfBlank(filterConfig.getInitParameter(SPRING_CONTEXT_NAME_INIT_PARAM_KEY),
                WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
    }

    @Override
    public RequestAuthenticator get() {
        AuthenticationContext authContext = getAuthenticationContext();

        return getRequestAuthenticatorFactory().create(authContext);
    }

    private RequestAuthenticatorFactory getRequestAuthenticatorFactory() {
        return getBeanForClass(RequestAuthenticatorFactory.class).orElse(new RequestAuthenticatorFactory());
    }

    private AuthenticationContext getAuthenticationContext() {
        return getBeanForClass(AuthenticationContext.class)
                .orElseThrow(() -> new IllegalStateException(
                        "Unable to find unique bean of type " + AuthenticationContext.class.getName() + " " +
                                "in your Spring configuration"
                ));
    }

    private <T> Optional<T> getBeanForClass(Class<T> klass) {
        try {
            T bean = cast(springContext.getBean(klass));
            return Optional.of(bean);
        } catch (NoSuchBeanDefinitionException ex) {
            return Optional.empty();
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T cast(Object obj) {
        return (T) obj;
    }
}
