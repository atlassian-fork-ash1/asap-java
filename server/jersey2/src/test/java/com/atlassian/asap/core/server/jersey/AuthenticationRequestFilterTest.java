package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.jersey.test.ResourceWithPackageAsap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Optional;

import static com.atlassian.asap.asap.AsapConstants.MAX_TRANSIENT_FAILURES_RETRIES;
import static com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE;
import static com.google.common.collect.Sets.newHashSet;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class AuthenticationRequestFilterTest {
    private static final String VALID_JWT = "Bearer valid_jwt";
    private static final String INVALID_JWT_TRANSIENT = "Bearer transient_invalid_jwt";
    private static final String INVALID_JWT_PERMANENT = "Bearer permanent_invalid_jwt";

    @SuppressWarnings("ThrowableInstanceNeverThrown")
    private static final TransientAuthenticationFailedException TRANSIENT_AUTHENTICATION_FAILED_EXCEPTION
            = new TransientAuthenticationFailedException(INVALID_JWT_TRANSIENT, null, null, null);

    @SuppressWarnings("ThrowableInstanceNeverThrown")
    private static final PermanentAuthenticationFailedException PERMANENT_AUTHENTICATION_FAILED_EXCEPTION
            = new PermanentAuthenticationFailedException(INVALID_JWT_PERMANENT, null);

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private RequestAuthenticator authenticator;
    @Mock
    private ResourceInfo resourceInfo;
    @Mock
    private FailureHandler failureHandler;
    @Mock
    private ContainerRequestContext context;
    @Mock
    private Jwt jwt;
    @Mock
    private JwtSecurityContextFactory jwtSecurityContextFactory;
    @Mock
    private JwtSecurityContext newSecurityContext;
    @Mock
    private SecurityContext previousSecurityContext;

    private AuthenticationRequestFilter filter;

    @Before
    public void setUp() throws AuthenticationFailedException {
        String audience = "presence-test";
        JwtClaims claims = mock(JwtClaims.class);
        when(claims.getAudience()).thenReturn(newHashSet(audience));
        when(claims.getIssuer()).thenReturn("presence-test");
        when(claims.getSubject()).thenReturn(Optional.empty());
        when(jwt.getClaims()).thenReturn(claims);

        when(authenticator.authenticateRequest(VALID_JWT)).thenReturn(jwt);

        when(authenticator.authenticateRequest(INVALID_JWT_TRANSIENT))
                .thenThrow(TRANSIENT_AUTHENTICATION_FAILED_EXCEPTION);

        when(authenticator.authenticateRequest(INVALID_JWT_PERMANENT))
                .thenThrow(PERMANENT_AUTHENTICATION_FAILED_EXCEPTION);

        when(jwtSecurityContextFactory.createSecurityContext(jwt, previousSecurityContext)).thenReturn(newSecurityContext);

        when(context.getSecurityContext()).thenReturn(previousSecurityContext);

        filter = new AuthenticationRequestFilter(authenticator, failureHandler, jwtSecurityContextFactory);
        filter.resourceInfo = resourceInfo;
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithValidJwtOnProtectedResourceMethod()
            throws IOException, AuthenticationFailedException, NoSuchMethodException {
        Class resourceClass = TestResourceWithMethodAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        mockAuthorization(VALID_JWT);

        filter.filter(context);

        verify(context).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context).setSecurityContext(newSecurityContext);
        verifyZeroInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithValidJwtOnProtectedResourceClass()
            throws IOException, AuthenticationFailedException, NoSuchMethodException {
        Class resourceClass = TestResourceWithClassAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        when(context.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(VALID_JWT);

        filter.filter(context);

        verify(context).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context).setSecurityContext(newSecurityContext);
        verify(context, never()).abortWith(any(Response.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithValidJwtOnExtendedProtectedResourceClass()
            throws IOException, AuthenticationFailedException, NoSuchMethodException {
        Class resourceClass = ExtendedTestResourceWithClassAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        mockAuthorization(VALID_JWT);

        filter.filter(context);

        verify(context).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context).setSecurityContext(newSecurityContext);
        verifyZeroInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithValidJwtOnPackageResourceClass() throws IOException, AuthenticationFailedException,
            NoSuchMethodException {
        Class resourceClass = ResourceWithPackageAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        mockAuthorization(VALID_JWT);

        filter.filter(context);

        verify(context).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context).setSecurityContext(newSecurityContext);
        verifyZeroInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithPermanentInvalidJwt() throws IOException, NoSuchMethodException {
        Class resourceClass = TestResourceWithMethodAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        mockAuthorization(INVALID_JWT_PERMANENT);

        filter.filter(context);

        verify(context, never()).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context, never()).setSecurityContext(newSecurityContext);
        verify(failureHandler).onPermanentAuthenticationFailure(context, PERMANENT_AUTHENTICATION_FAILED_EXCEPTION);
        verifyNoMoreInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithTransientInvalidJwtNoRetry() throws IOException, NoSuchMethodException {
        Class resourceClass = TestResourceWithMethodAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getDeclaredMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        when(failureHandler.onTransientAuthenticationFailure(any(), any())).thenReturn(false);

        mockAuthorization(INVALID_JWT_TRANSIENT);

        filter.filter(context);

        verify(context, never()).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context, never()).setSecurityContext(newSecurityContext);
        verify(failureHandler).onTransientAuthenticationFailure(context, TRANSIENT_AUTHENTICATION_FAILED_EXCEPTION);
        verifyNoMoreInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithTransientInvalidJwtWithOneRetry() throws IOException, NoSuchMethodException {
        Class resourceClass = TestResourceWithMethodAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getDeclaredMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        when(failureHandler.onTransientAuthenticationFailure(any(), any())).thenReturn(true, false);

        mockAuthorization(INVALID_JWT_TRANSIENT);

        filter.filter(context);

        verify(context, never()).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context, never()).setSecurityContext(newSecurityContext);
        verify(failureHandler, times(2)).onTransientAuthenticationFailure(context, TRANSIENT_AUTHENTICATION_FAILED_EXCEPTION);
        verifyNoMoreInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithTransientInvalidJwtAlwaysRetry() throws IOException, NoSuchMethodException {
        Class resourceClass = TestResourceWithMethodAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getDeclaredMethod("protectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        when(failureHandler.onTransientAuthenticationFailure(any(), any())).thenReturn(true);

        mockAuthorization(INVALID_JWT_TRANSIENT);

        filter.filter(context);

        verify(context, never()).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context, never()).setSecurityContext(newSecurityContext);
        verify(failureHandler, times(MAX_TRANSIENT_FAILURES_RETRIES - 1))
                .onTransientAuthenticationFailure(context, TRANSIENT_AUTHENTICATION_FAILED_EXCEPTION);
        verify(failureHandler).onAuthenticationFailure(context, TRANSIENT_AUTHENTICATION_FAILED_EXCEPTION);
        verifyNoMoreInteractions(failureHandler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithoutAsap() throws IOException, NoSuchMethodException {
        Class resourceClass = TestResourceWithMethodAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("unprotectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        filter.filter(context);

        verify(context, never()).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context, never()).setSecurityContext(newSecurityContext);
        verify(context, never()).abortWith(any(Response.class));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void filterWithoutAsapEnabled() throws IOException, NoSuchMethodException {
        Class resourceClass = TestResourceWithClassAsap.class;
        when(filter.resourceInfo.getResourceClass()).thenReturn(resourceClass);

        Method resourceMethod = resourceClass.getMethod("unprotectedGet");
        when(filter.resourceInfo.getResourceMethod()).thenReturn(resourceMethod);

        filter.filter(context);

        verify(context, never()).setProperty(AUTHENTIC_JWT_REQUEST_ATTRIBUTE, jwt);
        verify(context, never()).setSecurityContext(newSecurityContext);
        verifyZeroInteractions(failureHandler);
    }

    private void mockAuthorization(String headerValue) {
        when(context.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(headerValue);
    }

    @Asap(authorizedSubjects = "presence-test")
    private static class TestResourceWithClassAsap {
        public void protectedGet() {
        }

        @Asap(enabled = false)
        public void unprotectedGet() {
        }
    }

    private static class TestResourceWithMethodAsap {
        @Asap(authorizedSubjects = "presence-test")
        public void protectedGet() {
        }

        public void unprotectedGet() {
        }
    }

    private static class ExtendedTestResourceWithClassAsap extends TestResourceWithClassAsap {
    }

}
