package com.atlassian.asap.it;

import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.jersey.Asap;
import com.atlassian.asap.core.server.jersey.AuthenticationRequestFilter;
import com.atlassian.asap.core.server.jersey.AuthorizationRequestFilter;
import com.atlassian.asap.core.server.jersey.JwtSecurityContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.RuntimeDelegate;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

public class JerseyIntegrationTest extends BaseIntegrationTest {
    private static final String BASE_URL = "http://localhost:8080/";
    private static HttpServer httpServer;

    @Override
    protected URI getUrlForResourceName(String resourceName) {
        return URI.create(BASE_URL).resolve(resourceName);
    }

    @BeforeClass
    public static void startHttpServer() throws Exception {
        Application jerseyApp = new Application() {
            private final Set<Class<?>> resources = newHashSet(Controller.class);
            private final Set<Object> singletons = newHashSet(
                    AuthenticationRequestFilter.newInstance(new AuthenticationContext(AUDIENCE, PUBLIC_KEY_PROVIDER)),
                    AuthorizationRequestFilter.newInstance());

            @Override
            public Set<Class<?>> getClasses() {
                return resources;
            }

            @Override
            public Set<Object> getSingletons() {
                return singletons;
            }
        };
        // create a new server listening at port 8080
        httpServer = HttpServer.create(new InetSocketAddress(URI.create(BASE_URL).getPort()), 0);

        // create a handler wrapping the JAX-RS application
        HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(jerseyApp, HttpHandler.class);

        // map JAX-RS handler to the server root
        httpServer.createContext(URI.create(BASE_URL).getPath(), handler);

        // start the server
        httpServer.start();
    }

    @AfterClass
    public static void stopHttpServer() throws Exception {
        if (httpServer != null) {
            httpServer.stop(1);
            httpServer = null;
        }
    }

    @Path("/")
    @Asap(authorizedSubjects = "issuer1")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public static class Controller {

        @GET
        @Path(RESOURCE)
        public String resourceIssuer1(@Context ContainerRequestContext containerRequestContext) {
            // Due to https://stackoverflow.com/questions/39487860/jax-rs-custom-securitycontext-has-unexpected-type-when-injected-into-resource-m ,
            // we cannot receive directly the SecurityContext and downcast it because Jersey wraps it in its own class
            JwtSecurityContext jwtSecurityContext = (JwtSecurityContext) containerRequestContext.getSecurityContext();
            return jwtSecurityContext.getJwt().getClaims().getJwtId();
        }
    }
}
