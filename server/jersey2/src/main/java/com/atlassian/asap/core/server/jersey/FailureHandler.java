package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.core.JwtConstants;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * An extension point for customizing how the authentication and authorization failures are returned to the client.
 */
@SuppressWarnings("WeakerAccess")
public interface FailureHandler {
    /**
     * Handles when the Jwt token cannot validated, because of a possibly transient error (like a communication
     * failure or 500 error response). In this case a retry maybe possible.
     *
     * <p>Iif no retry is to be attempted (returning {@code false}, then the request should be aborted using the
     * {@link ContainerRequestContext} instance.
     *
     * <p>By default, this method simply delegate to
     * {@link #onAuthenticationFailure(ContainerRequestContext, AuthenticationFailedException)} and returns
     * {@code false} in order to NOT retry.
     *
     * @param context The request context
     * @param e       The {@link TransientAuthenticationFailedException}
     * @return {@code true} if the authentication should be retried, {@code false} otherwise.
     * @see #onPermanentAuthenticationFailure(ContainerRequestContext, PermanentAuthenticationFailedException)
     * @see #onAuthenticationFailure(ContainerRequestContext, AuthenticationFailedException)
     * @since 2.9.0
     */
    default boolean onTransientAuthenticationFailure(ContainerRequestContext context,
                                                     TransientAuthenticationFailedException e) {
        onAuthenticationFailure(context, e);
        return false; // do not retry by default
    }

    /**
     * Handles when the Jwt token cannot validated, because of a permanent error (such as wrong header value,
     * incorrect signature, etc.).  Requests should be aborted using the {@link ContainerRequestContext} instance.
     *
     * <p>By default, this method simply delegate to
     * {@link #onAuthenticationFailure(ContainerRequestContext, AuthenticationFailedException)}
     *
     * @param context The request context
     * @param e       The {@link PermanentAuthenticationFailedException}
     * @see #onTransientAuthenticationFailure(ContainerRequestContext, TransientAuthenticationFailedException)
     * @see #onAuthenticationFailure(ContainerRequestContext, AuthenticationFailedException)
     * @since 2.9.0
     */
    default void onPermanentAuthenticationFailure(ContainerRequestContext context,
                                                  PermanentAuthenticationFailedException e) {
        onAuthenticationFailure(context, e);
    }

    /**
     * Handles when the Jwt token cannot be parsed or validated.  Requests should be aborted using the {@link
     * ContainerRequestContext} instance
     *
     * <p>Default implementation returns a {@link Response.Status#UNAUTHORIZED HTTP 401} with an empty body.
     *
     * <p>If you want to handle more sprecific {@link TransientAuthenticationFailedException}
     * and {@link PermanentAuthenticationFailedException}, respectively implement
     * {@link #onTransientAuthenticationFailure(ContainerRequestContext, TransientAuthenticationFailedException)}
     * and {@link #onPermanentAuthenticationFailure(ContainerRequestContext, PermanentAuthenticationFailedException)}.
     * <strong>Note</strong> you'll still need to implement this method to handle generic
     * {@link AuthenticationFailedException}s.
     *
     * @param context The request context
     * @param e       The {@link AuthenticationFailedException}
     * @since 2.9.0
     */
    default void onAuthenticationFailure(ContainerRequestContext context, AuthenticationFailedException e) {
        context.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE, JwtConstants.BEARER_AUTHENTICATION_SCHEME)
                .build());
    }

    /**
     * Handles when the Jwt token claims cannot be authorized.  Requests should be aborted using the {@link
     * ContainerRequestContext} instance
     *
     * @param context The request context
     * @param e       The {@link AuthorizationFailedException authorization exception}
     * @since 2.9.0
     */
    default void onAuthorizationFailure(ContainerRequestContext context, AuthorizationFailedException e) {
        context.abortWith(Response.status(Response.Status.FORBIDDEN).build());
    }
}
