package com.atlassian.asap.it;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.jersey.JwtAuth;
import com.atlassian.asap.core.server.jersey.JwtAuthProvider;
import com.google.common.collect.ImmutableMap;
import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.container.grizzly.GrizzlyServerFactory;
import com.sun.jersey.api.core.ClassNamesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.net.URI;
import java.util.Map;

public class JerseyIntegrationTest extends BaseIntegrationTest {
    private static SelectorThread thread;
    private static final String BASE_URL = "http://localhost:8080/";

    @Override
    protected URI getUrlForResourceName(String resourceName) {
        return URI.create(BASE_URL).resolve(resourceName);
    }

    @BeforeClass
    public static void startHttpServer() throws Exception {
        ResourceConfig rc = new ClassNamesResourceConfig(
                JwtAuthProvider.class,
                Controller.class,
                PermanentAuthenticationFailedExceptionMapper.class,
                TransientAuthenticationFailedExceptionMapper.class,
                AuthorizationFailedExceptionMapper.class
        );

        Map<Class, Object> injectables = ImmutableMap.<Class, Object>of(
                AuthenticationContext.class, new AuthenticationContext(AUDIENCE, PUBLIC_KEY_PROVIDER)
        );

        rc.getSingletons().add(JerseyTestUtil.ioCProviderFactoryfromMap(injectables));
        thread = GrizzlyServerFactory.create(BASE_URL, rc);
    }

    @AfterClass
    public static void stopHttpServer() throws Exception {
        if (thread != null) {
            thread.stopEndpoint();
            thread = null;
        }
    }

    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public static class Controller {

        @GET
        @Path(RESOURCE)
        public String resourceIssuer1(
                @JwtAuth(authorizedSubjects = {"issuer1"}) Jwt jwt
        ) {
            return jwt.getClaims().getJwtId();
        }
    }

    @Provider
    public static class PermanentAuthenticationFailedExceptionMapper implements ExceptionMapper<PermanentAuthenticationFailedException> {
        @Override
        public Response toResponse(PermanentAuthenticationFailedException exception) {
            return Response
                    .status(Response.Status.UNAUTHORIZED) // 401
                    .build();
        }
    }

    @Provider
    public static class TransientAuthenticationFailedExceptionMapper implements ExceptionMapper<TransientAuthenticationFailedException> {
        @Override
        public Response toResponse(TransientAuthenticationFailedException exception) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR) // 500
                    .build();
        }
    }

    @Provider
    public static class AuthorizationFailedExceptionMapper implements ExceptionMapper<AuthorizationFailedException> {
        @Override
        public Response toResponse(AuthorizationFailedException exception) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }
    }
}
