package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class IssuerAndSubjectAwareRequestAuthorizationFilterTest {
    @Test
    public void isAuthorizedForSubjectMatch() throws Exception {
        Map<String, Predicate<String>> rules = ImmutableMap.of(
                "good/issuer", "good/subject"::equals
        );
        IssuerAndSubjectAwareRequestAuthorizationFilter filter = new IssuerAndSubjectAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("good/issuer", "good/subject");

        assertThat(filter.isAuthorized(null, jwt), is(true));
    }

    @Test
    public void isAuthorizedForServiceWithNoSubject() throws Exception {
        Map<String, Predicate<String>> rules = ImmutableMap.of(
                "good/issuer", "good/issuer"::equals
        );
        IssuerAndSubjectAwareRequestAuthorizationFilter filter = new IssuerAndSubjectAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("good/issuer", null);

        assertThat(filter.isAuthorized(null, jwt), is(true));
    }

    @Test
    public void isNotAuthorizedForGoodIssuerNoMatch() throws Exception {
        Map<String, Predicate<String>> rules = ImmutableMap.of(
                "good/issuer", "good/subject"::equals
        );
        IssuerAndSubjectAwareRequestAuthorizationFilter filter = new IssuerAndSubjectAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("good/issuer", "bad/subject");

        assertThat(filter.isAuthorized(null, jwt), is(false));
    }

    @Test
    public void isNotAuthorizedForBadIssuerGoodSubject() throws Exception {
        Map<String, Predicate<String>> rules = ImmutableMap.of(
                "good/issuer", "good/subject"::equals
        );
        IssuerAndSubjectAwareRequestAuthorizationFilter filter = new IssuerAndSubjectAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("bad/issuer", "good/subject");

        assertThat(filter.isAuthorized(null, jwt), is(false));
    }

    @Test
    public void issuerConstructorDisallowsImpersonation() throws Exception {
        Set<String> issuers = ImmutableSet.of("good/issuer", "another/good/issuer");
        IssuerAndSubjectAwareRequestAuthorizationFilter filter = IssuerAndSubjectAwareRequestAuthorizationFilter.issuers(issuers);
        Jwt badJwt = getJwt("bad/issuer", null);
        Jwt goodJwt = getJwt("good/issuer", "good/issuer");
        Jwt crossJwt = getJwt("good/issuer", "another/good/issuer");

        assertThat("the issuer is invalid", filter.isAuthorized(null, badJwt), is(false));
        assertThat("the issuer is valid", filter.isAuthorized(null, goodJwt), is(true));
        assertThat("the issuer is valid, but is impersonating another issuer",
                filter.isAuthorized(null, crossJwt), is(false));
    }

    static Jwt getJwt(String issuer, String subject) {
        return JwtBuilder.newJwt()
                .issuer(issuer)
                .subject(Optional.ofNullable(subject))
                .keyId("ignored")
                .audience("ignored")
                .build();
    }
}

