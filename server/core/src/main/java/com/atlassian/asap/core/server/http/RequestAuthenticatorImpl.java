package com.atlassian.asap.core.server.http;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.JwtConstants;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.validator.JwtValidator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;

public class RequestAuthenticatorImpl implements RequestAuthenticator {
    private static final Logger logger = LoggerFactory.getLogger(RequestAuthenticatorImpl.class);

    private final JwtValidator jwtValidator;

    public RequestAuthenticatorImpl(JwtValidator jwtValidator) {
        this.jwtValidator = Objects.requireNonNull(jwtValidator);
    }

    @Override
    public Jwt authenticateRequest(String authorizationHeader) throws AuthenticationFailedException {
        if (StringUtils.isBlank(authorizationHeader)) {
            throw new PermanentAuthenticationFailedException("Authorization header is missing");
        }

        if (!authorizationHeader.startsWith(JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX)) {
            throw new PermanentAuthenticationFailedException("Authorization header is not in the expected format. Expected format is 'Bearer <jwt token>'");
        }

        String serializedJwt = StringUtils.removeStart(authorizationHeader, JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX);
        try {
            return jwtValidator.readAndValidate(serializedJwt);
        } catch (PublicKeyNotFoundException e) {
            // log at debug because this can be caused by invalid input
            logger.debug("Public key not found when authenticating request: {}", e.getKeyId(), e);
            // we generally deliberately choose NOT to chain exceptions or their messages (to avoid leaking sensitive
            // private details into the caller's logs).  However PublicKeyNotFoundException messages should relate to
            // already publicly available information, so preserving the message here should be safe.
            final Optional<String> issuer = getUnverifiedIssuer(serializedJwt);
            throw new PermanentAuthenticationFailedException(
                    format("Public key not found when authenticating request from %s: %s",
                            formatIssuer(issuer), e.getMessage()),
                    issuer.orElse(null));
        } catch (CannotRetrieveKeyException e) { // a failure when retrieving the key
            // log at error because this indicates a system failure
            logger.error("Error retrieving key required to authenticate request", e);
            // we generally deliberately choose NOT to chain exceptions or their messages (to avoid leaking sensitive
            // private details into the caller's logs).  However CannotRetrieveKeyException messages should relate to
            // already publicly available information, so preserving the message here should be safe.
            final Optional<String> issuer = getUnverifiedIssuer(serializedJwt);
            throw new TransientAuthenticationFailedException(
                    format("Failed to retrieve the key required to authenticate request from %s: %s",
                            formatIssuer(issuer), e.getMessage()),
                    e.getKeyId().orElse(null),
                    e.getKeyUri().orElse(null),
                    issuer.orElse(null));
        } catch (InvalidTokenException e) {
            // log at debug because this can be caused by invalid input
            logger.debug("Failed to authenticate request", e);
            // we deliberately choose NOT to chain exceptions or their messages (to avoid leaking sensitive private
            // details into the caller's logs).  However, that leaves the caller totally unawares as to what went wrong.
            // A compromise here is to propagate some safe details from the exception, being the class name, and,
            // possibly, depending on the type of exception, some further information about the failure.
            final Optional<String> issuer = getUnverifiedIssuer(serializedJwt);
            throw new PermanentAuthenticationFailedException(
                    format("Failed to authenticate request from %s: %s", formatIssuer(issuer), e.getSafeDetails()),
                    issuer.orElse(null));
        }
    }

    private Optional<String> getUnverifiedIssuer(String serializedJwt) {
        return jwtValidator.determineUnverifiedIssuer(serializedJwt);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private static String formatIssuer(Optional<String> issuer) {
        return issuer.map(iss -> iss + " (issuer not verified)").orElse("unknown issuer");
    }
}
