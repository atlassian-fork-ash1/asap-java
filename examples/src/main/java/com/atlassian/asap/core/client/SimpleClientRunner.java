package com.atlassian.asap.core.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

/**
 * A command line interface for {@link SimpleClient}.
 */
public class SimpleClientRunner {
    public static final String ISSUER_SYSPROP = "asap.client.issuer";
    public static final String KEYID_SYSPROP = "asap.client.keyId";
    public static final String AUDIENCE_SYSPROP = "asap.client.audience";
    public static final String PRIVATE_KEY_SYSPROP = "asap.client.privateKey";
    public static final String RESOURCE_SERVER_URL_SYSPROP = "asap.client.resource.server.url";

    private static final Logger logger = LoggerFactory.getLogger(SimpleClientRunner.class);

    /**
     * Main function to run the client.
     *
     * @param args command line arguments
     * @throws Exception if something went wrong
     */
    public static void main(String[] args) throws Exception {
        String issuer = defaultIfBlank(System.getProperty(ISSUER_SYSPROP), "issuer1");
        String keyId = defaultIfBlank(System.getProperty(KEYID_SYSPROP), "issuer1/rsa-key-for-tests");
        String audience = defaultIfBlank(System.getProperty(AUDIENCE_SYSPROP), "test-resource-server");
        URI privateKeyBaseUrl = URI.create(defaultIfBlank(System.getProperty(PRIVATE_KEY_SYSPROP), "classpath:/privatekeys/"));
        URI resourceServerUrl = URI.create(defaultIfBlank(System.getProperty(RESOURCE_SERVER_URL_SYSPROP), "http://localhost:8080/"));

        SimpleClient simpleClient = new SimpleClient(issuer, keyId, audience, privateKeyBaseUrl);

        logger.info("Client Initialised with: Issuer: {}, KeyID: {}, Audience: {}, pkPath: {}, Resource Server: {}",
                issuer, keyId, audience, privateKeyBaseUrl, resourceServerUrl);

        simpleClient.execute(resourceServerUrl);
    }

}
