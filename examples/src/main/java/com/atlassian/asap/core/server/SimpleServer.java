package com.atlassian.asap.core.server;

import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.filter.AbstractRequestAuthenticationFilter;
import com.atlassian.asap.core.server.filter.WhitelistRequestAuthorizationFilter;
import com.atlassian.asap.core.server.http.RequestAuthenticatorImpl;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;
import com.google.common.collect.ImmutableSet;
import com.sun.grizzly.http.SelectorThread;
import com.sun.grizzly.http.servlet.ServletAdapter;
import com.sun.jersey.api.container.grizzly.GrizzlyServerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Set;

/**
 * A simple server for demonstration purposes. Only accepts valid requests from some issuers and subjects.
 */
public class SimpleServer {
    private static Logger logger = LoggerFactory.getLogger(SimpleServer.class);

    private final int port;

    private final String publicKeyBaseUrl;
    private final String audience;
    private final Set<String> authorizedSubjects;
    private final Set<String> authorizedIssuers;

    private SelectorThread thread;

    public SimpleServer(int port, String publicKeyBaseUrl, String audience,
                        Set<String> authorizedSubjects, Set<String> authorizedIssuers) {
        this.port = port;
        this.publicKeyBaseUrl = publicKeyBaseUrl;
        this.audience = audience;
        this.authorizedIssuers = ImmutableSet.copyOf(authorizedIssuers);
        this.authorizedSubjects = ImmutableSet.copyOf(authorizedSubjects);
    }

    public URI getUrl() {
        return URI.create("http://localhost:" + port);
    }

    /**
     * Start the server.
     *
     * @throws Exception if the server fails to start
     */
    public void start() throws Exception {
        ServletAdapter servletAdapter = new ServletAdapter();

        servletAdapter.setServletInstance(new HelloWorldServlet());

        servletAdapter.addFilter(
                newAuthenticationFilter(),
                "authenticationFilter",
                Collections.emptyMap()
        );
        servletAdapter.addFilter(
                newAuthorizationFilter(),
                "authorizationFilter",
                Collections.emptyMap()
        );

        thread = GrizzlyServerFactory.create(getUrl(), servletAdapter);
    }

    /**
     * Stop the server.
     */
    public void stop() {
        thread.stopEndpoint();
    }

    private Filter newAuthenticationFilter() {
        JwtValidator jwtValidator = JwtValidatorImpl.createDefault(audience, publicKeyBaseUrl);
        final RequestAuthenticator requestAuthenticator = new RequestAuthenticatorImpl(jwtValidator);
        return new AbstractRequestAuthenticationFilter() {
            @Override
            protected RequestAuthenticator getRequestAuthenticator(FilterConfig filterConfig) {
                return requestAuthenticator;
            }
        };
    }

    private Filter newAuthorizationFilter() {
        return new WhitelistRequestAuthorizationFilter(authorizedSubjects, authorizedIssuers);
    }

    private static class HelloWorldServlet extends HttpServlet {
        private static final long serialVersionUID = -134479483378982999L;

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            doGet(req, resp);
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            logger.info("Successfully authenticated request!");
            resp.getWriter().println("Hello World!");
        }
    }
}
